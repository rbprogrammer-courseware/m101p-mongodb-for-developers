#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader "Importing data."
mongoimport --drop -d students -c grades grades.ef42a2b3e7ff.json

printheader "Asserting data was imported correctly."
EXPECTED=800
ACTUAL=$(mongo --quiet localhost:27017/students <<EOF
    db.grades.count()
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "DB grades do not match"

printheader "Running script to remove the lowest homework."
python ./remove-lowest-homework.py

printheader "Asserting data was processed correctly."
EXPECTED=600
ACTUAL=$(mongo --quiet localhost:27017/students <<EOF
    db.grades.count()
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "DB grades do not match"

printheader "Finding 101st best grade."
EXPECTED='{ "_id" : ObjectId("50906d7fa3c412bb040eb709"), "student_id" : 100, "type" : "homework", "score" : 88.50425479139126 }'
ACTUAL=$(mongo --quiet localhost:27017/students <<EOF
    db.grades.find().sort( { 'score' : -1 } ).skip( 100 ).limit( 1 )
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "101st best grade does not match"

printheader "Displaying top 5 documents."
mongo --quiet localhost:27017/students <<EOF
db.grades \
    .find( { }, { 'student_id' : 1, 'type' : 1, 'score' : 1, '_id' : 0 }) \
    .sort( { 'student_id' : 1, 'score' : 1, } ) \
    .limit( 5 )
EOF

printheader "Finding student with highest average in the class."
mongo --quiet localhost:27017/students <<EOF
db.grades.aggregate( \
    { '\$group' : { \
        '_id' : '\$student_id', \
        'average' : { \$avg : '\$score' } } }, \
    { '\$sort' : { 'average' : -1 } }, \
    { '\$limit' : 1 } )
EOF

printheader "Shutting down the database."
mongod --dbpath ${DBPATH} --shutdown
killall mongod

printheader "Deleting the database files."
rm -rf ${DBPATH}

