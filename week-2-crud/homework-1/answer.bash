#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader "Importing data."
mongoimport --drop -d students -c grades grades.ef42a2b3e7ff.json

printheader "Asserting data was imported correctly."
EXPECTED=800
ACTUAL=$(mongo --quiet localhost:27017/students <<EOF
    db.grades.count()
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "DB grades"

printheader "Determining answer, which will be the student_id."
mongo --quiet localhost:27017/students <<EOF
db.grades \
    .find({type: 'exam', score : {'\$gte':65}}) \
    .sort({score:1}) \
    .limit(1) \
    .pretty()
EOF

printheader "Shutting down the database."
mongod --dbpath ${DBPATH} --shutdown
killall mongod

printheader "Deleting the database files."
rm -rf ${DBPATH}

