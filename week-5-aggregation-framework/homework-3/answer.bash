#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader 'Copying zip file.'
ZIP_NAME=chapter_5_aggregation_framework.f05059ebb3a5.zip
cp -v "../${ZIP_NAME}" .

printheader 'Unzipping the grade data.'
GRADES_DIR="Small_grades_file"
GRADES_FILE="${GRADES_DIR}/grades.json"
unzip "${ZIP_NAME}" "${GRADES_FILE}"

printheader 'Deleting zip file.'
rm -fv "${ZIP_NAME}"

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Importing the grade data.'
GRADE_DB_NAME=test
mongoimport --drop -d "${GRADE_DB_NAME}" -c grades "${GRADES_FILE}"

printheader 'Deleting the grade data file.'
rm -rfv "${GRADES_DIR}"

printheader "Calculating lowest class average."
EXPECTED='{ "_id" : { "class_id" : 2 }, "class_avg" : 37.61742117387635 }'
ACTUAL=$(mongo --quiet "${GRADE_DB_NAME}" <<EOF
    db.grades.aggregate([
        {'\$unwind': '\$scores'},
        {'\$match': {'\$or': [
            {'scores.type': 'exam'},
            {'scores.type': 'homework'},
        ]}},
        {'\$group': {
           '_id': {student_id: '\$student_id', class_id: '\$class_id'},
           student_avg: {'\$avg': '\$scores.score'},
        }},
        {'\$group': {
           '_id': {class_id: '\$_id.class_id'},
           class_avg: {'\$avg': '\$student_avg'},
        }},
        {'\$sort': {class_avg: 1}},
        {'\$limit': 1},
    ])
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "Lowest class average does not match"

printheader "Calculating highest class average."
ANSWER=$(mongo --quiet "${GRADE_DB_NAME}" <<EOF
    db.grades.aggregate([
        {'\$unwind': '\$scores'},
        {'\$match': {'\$or': [
            {'scores.type': 'exam'},
            {'scores.type': 'homework'},
        ]}},
        {'\$group': {
           '_id': {student_id: '\$student_id', class_id: '\$class_id'},
           student_avg: {'\$avg': '\$scores.score'},
        }},
        {'\$group': {
           '_id': {class_id: '\$_id.class_id'},
           class_avg: {'\$avg': '\$student_avg'},
        }},
        {'\$sort': {class_avg: -1}},
        {'\$limit': 1},
    ])
EOF
)

cleanupmongo

printheader 'And the answer is...'
echo "${ANSWER}"
