#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader 'Copying zip file.'
ZIP_NAME=chapter_5_aggregation_framework.f05059ebb3a5.zip
cp -v "../${ZIP_NAME}" .

printheader 'Unzipping zip code data.'
ZIP_CODE_FILE="small_zips.json"
unzip "${ZIP_NAME}" "${ZIP_CODE_FILE}"

printheader 'Deleting zip file.'
rm -fv "${ZIP_NAME}"

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Importing zip code data.'
ZIP_CODE_DB_NAME=test
mongoimport --drop -d "${ZIP_CODE_DB_NAME}" -c zips "${ZIP_CODE_FILE}"

printheader 'Deleting zip code file.'
rm -rfv "./${ZIP_CODE_FILE}"

printheader "Calculating average population for CT and NJ."
EXPECTED='{ "_id" : "abc", "avg_pop" : 38176.63636363636 }'
ACTUAL=$(mongo --quiet "${ZIP_CODE_DB_NAME}" <<EOF
    db.zips.aggregate([
        {'\$group': {
           '_id': {state: '\$state', city: '\$city'},
           pop: {'\$sum': '\$pop'}
        }},
        {'\$match': {pop: {'\$gt': 25000}}},
        {'\$match': {'\$or': [{'_id.state': 'CT'}, {'_id.state': 'NJ'}]}},
        {'\$group': {
           '_id': 'abc',
           avg_pop: {'\$avg': '\$pop'}
        }},
    ])
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "Average population of CT+NJ does not match"

printheader "Calculating average population for CA and NY."
ANSWER=$(mongo --quiet "${ZIP_CODE_DB_NAME}" <<EOF
    db.zips.aggregate([
        {'\$group': {
           '_id': {state: '\$state', city: '\$city'},
           pop: {'\$sum': '\$pop'}
        }},
        {'\$match': {pop: {'\$gt': 25000}}},
        {'\$match': {'\$or': [{'_id.state': 'CA'}, {'_id.state': 'NY'}]}},
        {'\$group': {
           '_id': 'abc',
           avg_pop: {'\$avg': '\$pop'}
        }},
    ])
EOF
)

cleanupmongo

printheader 'And the answer is...'
echo "${ANSWER}"
