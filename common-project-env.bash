#!/usr/bin/env bash

export M101P_BASE_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

MONGODB_DIR="${M101P_BASE_DIR}/mongodb-linux-x86_64-3.2.10"
MONGODB_BIN_DIR="${MONGODB_DIR}/bin"
export PATH="${MONGODB_BIN_DIR}:${PATH}"

export DBPATH=./dbpath

function cdhw() {
    WEEK=$1
    HOMEWORK=$2
    cd ${M101P_BASE_DIR}/week-${WEEK}-*/homework-${HOMEWORK}
}
export -f cdhw

function cdfe() {
    QUESTION=$1
    if [ -d ${M101P_BASE_DIR}/week-7-final-exam/question-${QUESTION} ] ; then
        cd ${M101P_BASE_DIR}/week-7-final-exam/question-${QUESTION}
    else
        cd ${M101P_BASE_DIR}/week-7-final-exam/question-0${QUESTION}
    fi
}
export -f cdfe

function setupmongo() {
    pushd ${M101P_BASE_DIR}
    unzip mongodb-linux-x86_64-3.2.10.tgz
    popd
}
export -f setupmongo

function cleanupmongo() {
    printheader "Shutting down the database."
    mongod --dbpath ${DBPATH} --shutdown

    printheader "Deleting the database files."
    rm -rfv ${DBPATH}
}
export -f cleanupmongo

HEAD_COUNT=0
function printheader() {
    HEAD_COUNT=$((HEAD_COUNT + 1))
    echo
    echo '========================================'
    echo "(${HEAD_COUNT}) ${@}"
}
export -f printheader

function assertequals() {
    EXPECTED=$1
    ACTUAL=$2
    MESSAGE=$3
    if [ "x${EXPECTED}" != "x${ACTUAL}" ] ; then
        echo -n "ERROR: Expected '${EXPECTED}' does not equal actual '${ACTUAL}'"
        [ -n "${MESSAGE}" ] && echo -n ": ${MESSAGE}"
        echo '.'
        false
        exit
    fi
}
export -f assertequals
