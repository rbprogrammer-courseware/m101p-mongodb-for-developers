#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Restoring Enron data'
ZIP_NAME=../question-01/enron.zip
ENRON_DUMP_DIR=dump
mongorestore --drop "${ENRON_DUMP_DIR}"
unzip "${ZIP_NAME}"
rm -rfv "${ENRON_DUMP_DIR}"

printheader "Finding most communicated pairs of people."
ENRON_DB_NAME=enron
ANSWER=$(mongo --quiet "${ENRON_DB_NAME}" <<EOF
    db.messages.aggregate([
        {'\$match': {'headers.To': {'\$ne': 'recipients@enron.com'}}},
        {'\$unwind': '\$headers.To'},
        {'\$group': {
            '_id': {'from': '\$headers.From', 'to': '\$headers.To'},
            'email_count': {'\$sum': 1}
            }},
       {'\$sort': {'email_count': -1}},
       {'\$limit': 1},
    ])
EOF
)

cleanupmongo

printheader 'And the answer is...'
echo "${ANSWER}"
