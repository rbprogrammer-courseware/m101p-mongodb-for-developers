#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader 'Copying zip file.'
ZIP_NAME=chapter_4_performance.06c44976aabe.zip
cp -v "../${ZIP_NAME}" .

printheader 'Unzipping profile data.'
PROFILE_FILE="sysprofile.json"
unzip "${ZIP_NAME}" "${PROFILE_FILE}"

printheader 'Deleting zip file.'
rm -fv "${ZIP_NAME}"

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Importing test data.'
mongoimport --drop -d m101 -c profile "./${PROFILE_FILE}"

printheader 'Deleting profile data.'
rm -rfv "./${PROFILE_FILE}"

printheader "Searching dataset for longest running operation."
mongo --quiet m101 <<EOF
    // db.profile.distinct("ns", {})
    db.profile.find({ns: "school2.students"}, {millis:1}).sort({millis:-1}).limit(1)
EOF

cleanupmongo


