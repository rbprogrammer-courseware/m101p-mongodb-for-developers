#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader 'Making data directories'
DATA_TOP_LEVEL_DIR="./data"
mkdir -pv ${DATA_TOP_LEVEL_DIR}/rs{1,2,3}

printheader 'Starting mongod replica set.'
START_PORT=27017
function start_mongod_repl_set() {
    INSTANCE_NUM=$1
    PORT=$(( START_PORT + INSTANCE_NUM - 1 ))
    mongod \
        --replSet m101 \
        --logpath "${DATA_TOP_LEVEL_DIR}/rs${INSTANCE_NUM}.log" \
        --dbpath "${DATA_TOP_LEVEL_DIR}/rs${INSTANCE_NUM}" \
        --port ${PORT} \
        --smallfiles \
        --oplogSize 64 \
        --fork
}
start_mongod_repl_set 1
start_mongod_repl_set 2
start_mongod_repl_set 3

printheader 'Initializing replica set.'
mongo --quiet --port ${START_PORT} <<EOF
    config = { _id: "m101", members:[
        { _id : 0, host : "localhost:27017"},
        { _id : 1, host : "localhost:27018"},
        { _id : 2, host : "localhost:27019"},
    ]};
    rs.initiate(config);
EOF

printheader 'Sleeping to give replica set time to replicate configuration.'
sleep 10s

printheader 'Determining replica set status.'
mongo --quiet --port ${START_PORT} <<EOF
    rs.status()
EOF

printheader 'Copying zip file.'
ZIP_NAME=chapter_6_application_engineering.5a0d0cc948a1.zip
cp -v "../${ZIP_NAME}" .

printheader 'Unzipping the homework validator.'
HW_VALIDATOR="validate__hw6.5_m101p_52b36d2fe2d423678d3b9d7c.py"
unzip "${ZIP_NAME}" "${HW_VALIDATOR}"

printheader 'Deleting zip file.'
rm -fv "${ZIP_NAME}"

printheader 'Running homework validator.'
python ${HW_VALIDATOR}

printheader 'Deleting validator script.'
rm -fv ${HW_VALIDATOR}

printheader 'Stopping mongod replica set.'
function stop_mongod_repl_set() {
    INSTANCE_NUM=$1
    PORT=$(( START_PORT + INSTANCE_NUM - 1 ))
    mongod --dbpath ".${DATA_TOP_LEVEL_DIR}/rs${INSTANCE_NUM}" --shutdown
}
stop_mongod_repl_set 1
stop_mongod_repl_set 2
stop_mongod_repl_set 3

printheader 'Deleting data directories.'
rm -rfv "${DATA_TOP_LEVEL_DIR}"
